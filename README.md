# pomotrack README

Pomotrack is a simple timer extension for VS Code.

## Features

To use this extension, run 'Pomostart' command and look at the bottom left edge of your VS Code window. Pomodoro timer should start there


## Extension Settings

Include if your extension adds any VS Code settings through the `contributes.configuration` extension point.

For example:

This extension contributes the following settings:

* `pomotrack.workinterval`: set this value to desired amount of seconds for timer


### 1.0.0

Initial release of Pomotrack


