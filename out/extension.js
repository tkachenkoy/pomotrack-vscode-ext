"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
const vscode = require("vscode");
const timer_1 = require("./timer");
let pomo_statusbaritem;
let pomodoro_run_event_start;
function activate(context) {
    let timer = new timer_1.default();
    const settings_interval = vscode.workspace.getConfiguration('pomotrack').get('workinterval');
    const pomodoroInterval = Number(settings_interval) * 1000;
    const stopPomodoro = 'pomotrack.pomoStop';
    const runPomodoro = 'pomotrack.pomoRun';
    let timer_callback_fn = function (statusbar_item, stop_initiated) {
        // stop the timer if no second left
        statusbar_item.text = 'Pomodoro:	' + String(Math.trunc(pomodoroInterval / 1000) + timer.tickedTime) + ' s left';
        if (timer.tickedTime * 1000 <= -1 * pomodoroInterval) {
            statusbar_item.text = 'Pomodoro stopped (time is out)';
            statusbar_item.backgroundColor = new vscode.ThemeColor('statusBarItem.errorBackground');
            statusbar_item.command = runPomodoro;
            timer.stop();
            timer.reset(0);
            console.log('time is out');
        }
    };
    context.subscriptions.push(vscode.commands.registerCommand(stopPomodoro, () => {
        vscode.window.showInformationMessage(`Pomodoro -> stop`);
        pomo_statusbaritem.text = 'Pomodoro stopped manually';
        pomo_statusbaritem.backgroundColor = new vscode.ThemeColor('statusBarItem.errorBackground');
        pomo_statusbaritem.command = runPomodoro;
        timer.stop();
        timer.reset(0);
    }));
    context.subscriptions.push(vscode.commands.registerCommand(runPomodoro, () => {
        vscode.window.showInformationMessage(`Pomodoro -> start`);
        pomodoro_run_event_start = Date();
        pomo_statusbaritem.text = 'Pomodoro started ' + pomodoro_run_event_start;
        pomo_statusbaritem.backgroundColor = new vscode.ThemeColor('statusBarItem.warningBackground');
        pomo_statusbaritem.command = stopPomodoro;
        timer.start(() => timer_callback_fn(pomo_statusbaritem, false));
    }));
    let disposable = vscode.commands.registerCommand('pomotrack.pomoStart', () => {
        // The code you place here will be executed every time your command is executed
        // Display a message box to the user
        vscode.window.showInformationMessage('Pomodoro timer has started!');
        pomo_statusbaritem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 999);
        pomo_statusbaritem.backgroundColor = new vscode.ThemeColor('statusBarItem.warningBackground');
        pomodoro_run_event_start = Date();
        pomo_statusbaritem.text = 'Pomodoro started ' + pomodoro_run_event_start;
        pomo_statusbaritem.command = stopPomodoro;
        pomo_statusbaritem.show();
        timer.start(() => timer_callback_fn(pomo_statusbaritem, false));
    });
    context.subscriptions.push(disposable);
}
exports.activate = activate;
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map